powerusers/hardware:
  Software:
    Annoyances:
      alienfx-tools(R)              Recommended for dell pc/laptops | recommended to remove all dell/alienware related software and use this instead
      OpenTabletDriver              open source, lightweight drawing tablet drivers
      
    QOL:
      powertoys(S)                  windows addons
      QTTabbar(s)                   Tabs for Windows Explorer
      copyq(S)(R)                   clipboard manager
      speedyfox(S)(R)               browser speed up | done through defragging the sql database for profile/userdata
      speedtest-cli(S)              internet speed test
      Alt-Drag/Alt-Snap(S)(R)       Allows for resizing, moving, and other windows controls using the alt-key | Alt-Snap is the newer version/fork
      ZoomIt(R)                     Zoom into your screen and draw on it | https://learn.microsoft.com/en-us/sysinternals/downloads/zoomit

    File Management:
      7zip(S)(R)                    archive extractor
      jdownloader(S)(R)             Download manager
      
    Disk Info/Help:
      ShutUp10(S)(R)                remove telemetry from windows10
      Windows10Debloater            Remove preinstalled apps | Might break something
      Bulk-Crap-Uninstaller(S)(R)   FOSS Uninstaller
      Bulk-Rename-Utility(S)(R)     Renaming tool
      wiztree(S)(R)                 disk space analyser
      windirstat(S)                 disk space analyser, superseeded by wiztree
      treesize(S)                   disk space analyser, alternative to wiztree
      crystaldiskinfo(S)            disk life/health checker
      unlocker                      sometimes files won't delete/rename/move  |  unlocker tries to bypass restrictions on a specified file
    
    Computers and Programming:
      git(S)                        Repository of snapshots | Version Control
      aria2(S)                      multi-source download utility
      Autohotkey(S)                 Make macros and hotkeys
      thefuck                       cli quick fix incorrect/errors in terminal commands
      
    Innounp(S)                  cli program unpacker
    croc                        send files to others
    dupeguru                    Find duplicate files
    unxutils(S)(R)              common utils from *nix based systems, recommended for those with a linux background, or are moving to linux
  
    https://ffprofile.com/      firefox customization for first install
  
  OS:
    rufus(S)(R)                 Make bootable install media
    qemu                        Emulator | virtual machine
    virtualbox(s)               Emulator |virtual machine
    Dolphin(S)                  Emulator | GameCude and Wii
    
  Hardware:
    nvcleanstall(S)(R)          get nvidia drivers without telemetry or extra unneeded software
    tinynvidiaupdatechecker     get nvidia update notifications, without need for geforce experience | privacy
    Everything(S)(R)            better windows search  |  search for any file on your system near instant, no indexing
    Hwinfo(S)                   hardware/system monitoring
    Core Temp(S)                Hardware Temperatures and Health
    speedfan(S)                 Control and monitor computer fans